# Beacon

Beyond: Beacon - Drupal module

Installs the Beacon Javascript on all pages on your site and allows for tracking of
listing search data, and tracking conversions.

https://beyondpricing.com


## Development

To setup a drupal template:

Drupal 9
```sh
composer create-project drupal/recommended-project beacon.drupal.9
cd beacon.drupal.9
ln -s $CODE/beyond/beacon/drupal web/modules/beacon
php web/core/scripts/drupal quick-start demo_umami
```


Drupal 7
```sh
composer create-project drupal-composer/drupal-project:7.x-dev -n beacon.drupal.7
cd beacon.drupal.7/web
ln -s $CODE/beyond/beacon/drupal web/sites/all/modules/beacon
php -S 127.0.0.1:8889
open 127.0.0.1:8889/install.php
```
